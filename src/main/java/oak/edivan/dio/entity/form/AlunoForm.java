package oak.edivan.dio.entity.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlunoForm {
    @NotBlank(message = "Preenchar o camp corramente.")
    @Size(min = 3, max = 50, message = "'${validatedValue}' precisa esta entre  {min} e {max} caracteres.")
    private String nome;
    @NotBlank(message = "Preenchar o campo corretamente")
    @CPF(message = "'${vallidatedValue}'  é inválido!")
    private String cpf;
    @NotBlank(message = "Preenchar o campo corretamnete.")
    @Size(min = 3, max = 50, message = "'${validatedValue}' precisa esta entre  {min} e {max} caracteres.")
    private String bairro;
    @NotNull(message = "Preenchar o campo corretamnete.")
    @Past(message = "Data '${validatedValue}' é invalida")
    private LocalDate dataDeNascimento;
}
