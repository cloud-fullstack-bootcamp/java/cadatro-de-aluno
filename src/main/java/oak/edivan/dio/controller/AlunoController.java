package oak.edivan.dio.controller;

import oak.edivan.dio.entity.Aluno;
import oak.edivan.dio.entity.AvaliacaoFisica;
import oak.edivan.dio.entity.form.AlunoForm;
import oak.edivan.dio.service.impl.AlunoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/alunos")
public class AlunoController {
    @Autowired
    private AlunoServiceImpl service;
    @PostMapping
    public Aluno create(@Valid @RequestBody AlunoForm form){
        return service.create(form);
    }

    @GetMapping
    public List<Aluno> getAll(){
        return service.getAll();
    }


    @GetMapping("/avaliacoes/{id}")
    public List<AvaliacaoFisica> getAllAvalicao(@PathVariable Long id){
        return service.getAllAvaliacaoFisica(id);
    }

}
