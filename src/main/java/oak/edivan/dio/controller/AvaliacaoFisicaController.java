package oak.edivan.dio.controller;

import oak.edivan.dio.entity.AvaliacaoFisica;
import oak.edivan.dio.entity.form.AvaliacaoFisicaForm;
import oak.edivan.dio.service.impl.IAvaliacaoFisicaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class AvaliacaoFisicaController {
    @Autowired
    private IAvaliacaoFisicaServiceImpl service;

    public AvaliacaoFisicaController(IAvaliacaoFisicaServiceImpl service) {
        this.service = service;
    }

    @PostMapping()
    public AvaliacaoFisica create(@RequestBody AvaliacaoFisicaForm form){
        return service.create(form);
    }
}
